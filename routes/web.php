<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "Hello Word";
});

Route::get('/student/{id?}', function ($id = 'no student provided') {
    return "Hello student ".$id;
})->name('student');

Route::get('/comment/{idid}', function ($idid) {
    return view('comment',['id'=> $idid]);
})->name('comment');

Route::get('/customers/{cnumber?}', function ($cnumber = 'No customer was provided') {
    if($cnumber=='No customer was provided')
        return view('nocustomers',['cnumber'=>$cnumber]);
        else
            return view('customers',['cnumber'=>$cnumber]);
})->name('customer');

Route::resource('todos','TodoController')->middleware('auth');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
